#!/bin/bash

#set -x  # for debugging
set -e  # shell will exit if a command fails

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

version='1.4'

usage(){
cat << EOF
usage: bash plasmidEC.sh [-i INPUT] [-o OUTPUT] [options]

Mandatory arguments:
  -i INPUT		input .fasta or .gfa file.
  -o OUTPUT		output directory.

Optional arguments:
  -h 			 Display this help message and exit.
  -n NAME		 Name for output files. (Default: Basename of input file).
  -l LENGTH 		 Minimum length of contigs to be classified (Default: 1000)
  -g			 Write gplas2 formatted output.
  -m                     Use minority vote to classify contigs as plasmid-derived.
  -f			 Force overwriting of output directory.
  -v			 Display version and exit.

EOF
}

#set default values
classifiers='centrifuge,platon,rfplasmid'
threads=8
force='false'
gplas_output='false'
species='Escherichia coli'

#process flags provided
while getopts :i:o:l:n:fgmvh flag; do
	case $flag in
		i) input=$OPTARG;;
        o) out_dir=$OPTARG;;
        l) length=$OPTARG;;
		n) name=$OPTARG;;
		f) force='true';;
		g) gplas_output='true';;
		m) minority_vote='true';;
		v) echo "PlasmidEC v. $version." && exit 0;;
		h) usage && exit 0;;
	esac
done

#Establish default values for E.coli
centrifuge_database_path=${SCRIPT_DIR}/databases/centrifuge
centrifuge_database_name='chromosome_plasmid_db'
rfplasmid_model='Enterobacteriaceae'
classifiers='centrifuge,platon,rfplasmid'
echo "The prediction will be made for Escherichia coli plasmids with E. coli binary classifiers"

#when no flags are provided, display help message
if [ $OPTIND -eq 1 ]; then
	usage && exit 1
fi

#start plasmidEC
printf "PlasmidEC v. $version.\nUsing binary classifiers: $classifiers.\n"

#load user's conda base environment
CONDA_PATH=$(conda info | grep -i 'base environment' | awk '{print $4}')
source $CONDA_PATH/etc/profile.d/conda.sh || echo "Error: Unable to load conda base environment. Is conda installed?" || exit 1

#if input or output flags are not present or input is incorrect, write message and quit
[ -z $input ] && echo "Please provide the path to your input folder (-i)" && exit 1 
[ -z $out_dir ] && echo "Please provide the name of the output directory (-o)" && exit 1

#Check if input file is fasta or gfa format. Assign name to file.
if [[ $input == *.fasta ]]; then
	echo -e "\nFound input file at: $input"
	format="fasta"
	if [[ -z $name ]]; then
		name="$(basename -- $input .fasta)"
	else
		name=$name
	fi

elif [[ $input == *.gfa ]]; then
  echo -e "\nFound input file at: $input"
  echo "The file is in .gfa format. plasmidEC will convert it to FASTA format."
  format="gfa"
	if [[ -z $name ]]; then
        	name="$(basename -- $input .gfa)"
        else
        	name=$name
        fi
else
	echo "Error: No .fasta or .gfa file found at: $input" && exit 1
fi

#create output directory
if [[ -d $out_dir ]]; then
	if [[ $force = 'true' ]]; then
		rm -r $out_dir
		mkdir $out_dir
		mkdir $out_dir/logs
	else
		printf "\nOutput directory already exists: $out_dir\nUse the force option (-f) to overwrite.\n" && exit 1
	fi
else
	mkdir $out_dir
        mkdir $out_dir/logs
fi

#---Filter by length, move and (if required) convert format of input file.---
#Check if length is a valid value
if [[ -z "$length" ]]; then
    echo -e "\nPlasmidEC will classify only contigs larger than 1000bp. You can select a different cut-off using the -l flag"
    length=1000
else
#check if length is a positive value
  if [ "$length" > 0 ]; then
    echo -e "\nYou have selected to filter-out contigs smaller than: "${length}
  else
    echo -e "\nYou have selected an invalid value for -l. Please select a positive integer"
  fi
fi

#filter by contig size and create a new file (fitlered)
bash $SCRIPT_DIR/scripts/extract_nodes.sh -i ${input} -o ${out_dir} -l ${length} -f ${format} -n ${name}

#change the input to a new one
input=${out_dir}/${name}.fasta

#save list of conda envs already existing
envs=$(conda env list | awk '{print $1}' )

#create an environment for running r codes
if ! [[ $envs = *"plasmidEC_R"* ]]; then
        echo -e "\nCreating conda environment plasmidEC_R."
        conda create --name plasmidEC_R -c conda-forge r=4.1 --yes
        conda activate plasmidEC_R
        conda install -c bioconda bioconductor-biostrings=2.60.0 --yes
        conda install -c conda-forge r-tidyr=1.2.0 --yes
        conda install -c conda-forge r-plyr=1.8.6 --yes
        conda install -c conda-forge r-dplyr=1.0.7 --yes
fi


if ! [[ $envs = *"plasmidEC_centrifuge"* ]]; then
	echo -e "\nCreating conda environment plasmidEC_centrifuge."
	conda create --name plasmidEC_centrifuge python=3.6 --yes
	conda activate plasmidEC_centrifuge
	conda install centrifuge=1.0.4_beta -c bioconda --yes
fi

conda activate plasmidEC_centrifuge
bash $SCRIPT_DIR/scripts/run_centrifuge.sh -i $input -o $out_dir -t $threads -d ${centrifuge_database_path} -n ${centrifuge_database_name} -s "$species"
conda activate plasmidEC_R
echo -e "\nCreating Centrifuge final output..."
Rscript $SCRIPT_DIR/scripts/transform_centrifuge_output.R $out_dir $name 1>> ${out_dir}/logs/centrifuge_log.txt 2>> ${out_dir}/logs/centrifuge_err.txt

if ! [[ $envs = *"plasmidEC_platon"* ]]; then
	echo -e "\nCreating conda environment plasmidEC_platon."
	conda create --name plasmidEC_platon -c bioconda -c conda-forge  platon=1.6 --yes
fi
conda activate plasmidEC_platon
bash $SCRIPT_DIR/scripts/run_platon.sh -i $input -o $out_dir -t $threads -d $SCRIPT_DIR

if ! [[ $envs = *"plasmidEC_rfplasmid"* ]]; then
	echo -e "\nCreating conda environment plasmidEC_rfplasmid. This one might take sometime, please be patient..."
	conda create --name plasmidEC_rfplasmid -c bioconda -c conda-forge python=3.7 rfplasmid=0.0.18 --yes
	conda activate plasmidEC_rfplasmid
	rfplasmid --initialize
fi
conda activate plasmidEC_rfplasmid
bash $SCRIPT_DIR/scripts/run_rfplasmid.sh -i $input -o $out_dir -t $threads -s ${rfplasmid_model}

#Gather results
echo "Gathering results..."
bash $SCRIPT_DIR/scripts/gather_results.sh -i $input -c $classifiers -o $out_dir

#Check if minority vote option has been selected
if [[ $minority_vote = 'true' ]]; then
    plasmid_limit=0
    echo -e "\nYou have selected the -m flag. Minority vote for classifying contigs as plasmid will be applied"
else
    plasmid_limit=1
fi

#Combine results
conda activate plasmidEC_R
echo -e "\nCombining results..."
Rscript $SCRIPT_DIR/scripts/combine_results.R $out_dir $plasmid_limit $classifiers "false"

#put results in gplas2 format
if [[ $gplas_output = 'true' ]]; then
	#create a directory for the gplas output format
	mkdir $out_dir/gplas2_format
	echo -e "\nWriting gplas output..."
	echo -e "\n Find logs and errors at ${out_dir}/logs/gplas_conversion*\n"
	Rscript $SCRIPT_DIR/scripts/write_gplas_output.R $input $out_dir 1>> ${out_dir}/logs/gplas2_conversion_log.txt 2>> ${out_dir}/logs/gplas2_conversion_err.txt
fi
#write fasta file with plasmid contigs
echo -e "\nWriting plasmid contigs..."
bash $SCRIPT_DIR/scripts/write_plasmid_contigs.sh -i $input -o $out_dir

[ -f $out_dir/ensemble_output.csv ] && echo -e "\nPlasmidEC finished successfully. Output can be found in: $out_dir" && exit
