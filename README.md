<p align="center">
<img src="plasmidEC_logo.svg" alt="logo_package" width="600">
</p>

PlasmidEC is an ensemble of plasmid classification tools and allows prediction of plasmid contigs of _Escherichia coli_. 
A version of plasmidEC for multiple species is under development at [plasmidEC-allspecies](https://gitlab.com/mmb-umcu/plasmid-ec-allspecies).

PlasmidEC runs multiple binary classification tools that predict the origin of contigs (plasmid or chromosome). 
For each contig, it outputs the prediction given by the majority of the tools. 
PlasmidEC outcompetes individual classifiers, especially for contigs that contain antibiotic resistance genes.

## Table of contents
* [Requirements](#requirements)
* [Supported tools](#supported-tools)
* [Installation](#installation)
* [Usage](#usage)
    * [Input](#input)
    * [Compatibility with gplas2](#compatibility-with-gplas2)
    * [All options](#all-options)
* [Output files](#output-files)
* [Troubleshooting](#troubleshooting)
    * [Conda environments](#conda-environments)
    * [Database downloads](#database-downloads)  
* [Reference](#reference) 
* [Acknowledgements](#acknowledgements)

## Requirements
The only hard requirement to run plasmidEC is a conda installation (V >= 4.10.3). 
Upon the first run, plasmidEC will automatically create a number of conda environments, 
install dependencies and download the databases used by the tools. 
If preferred, and for debugging purposes, conda environments can be created separately and databases downloaded prior to running PlasmidEC. 
See [Conda environments](*conda-environments) and [Database downloads](*database-downloads).

Also note that running the individual tools requires a substantial amount of memory. 
We advise to allocate at least 16G of RAM. 

## Supported tools
- [Centrifuge](https://ccb.jhu.edu/software/centrifuge)
- [Platon](https://github.com/oschwengers/platon)
- [RFPlasmid](https://github.com/aldertzomer/RFPlasmid)

## Installation
Clone plasmidEC from github:
```
git clone https://gitlab.com/mmb-umcu/plasmidEC.git
```
Move to the new directory:
```
cd plasmidEC
```
Run plasmidEC:
```
bash plasmidEC.sh -i testdata/E_coli_test.fasta -o E_coli_test
```
Upon first run, plasmidEC will automatically install its dependencies via conda and download the databases used by the tools. 

*Note: Since two databases will be downloaded and three different tools are required, the installation of plasmidEC might take some time (~20 min), depending on your internet bandwith.*  

## Usage

### Input
As input, plasmidEC takes assembled contigs in **.fasta** format **or** an assembly graph in **.gfa** format. Such files can be obtained
with [SPAdes genome assembler](https://github.com/ablab/spades) or with [Unicycler](https://github.com/rrwick/Unicycler).

### Compatibility with gplas2
[gplas2](https://gitlab.com/mmb-umcu/gplas2) is a tool that accurately bins predicted plasmid contigs into individual plasmids.

By using the **-g** flag, plasmidEC provides an extra output file that can be directly used as an input for gplas2. 

For optimal performance of this feature, we advise to use an assembly graph (in **.gfa** format) as an input for plasmidEC. See an example command below:

```
bash plasmidEC.sh -i testdata/E_coli_graph.gfa -o E_coli_gplas2 -g
```
The gplas2-compatible output is a **tab separated** file, located at: ${output}/**gplas2_format**/${file_name}_plasmid_prediction.tab. See an example below:

```
head -n 10 E_coli_gplas2/gplas2_format/E_coli_graph_plasmid_prediction.tab
```

| Prob\_Chromosome | Prob\_Plasmid | Prediction | Contig\_name                              | Contig\_length |
| ---------------- | ------------- | ---------- | ----------------------------------------- | -------------- |
| 1                | 0             | Chromosome | S1\_LN:i:346767\_dp:f:0.9966562474408179  | 346767         |
| 1                | 0             | Chromosome | S10\_LN:i:175297\_dp:f:0.9360667247742771 | 175297         |
| 0.33             | 0.67          | Plasmid    | S100\_LN:i:1076\_dp:f:2.530236029051145   | 1076           |
| 1                | 0             | Chromosome | S101\_LN:i:1066\_dp:f:1.9988380278126159  | 1066           |
| 1                | 0             | Chromosome | S102\_LN:i:1030\_dp:f:2.0266855175827887  | 1030           |
| 1                | 0             | Chromosome | S11\_LN:i:173576\_dp:f:1.0807318234217165 | 173576         |
| 1                | 0             | Chromosome | S12\_LN:i:165545\_dp:f:1.0925719220847394 | 165545         |
| 1                | 0             | Chromosome | S13\_LN:i:158764\_dp:f:1.074893837075452  | 158764         |
| 1                | 0             | Chromosome | S14\_LN:i:154045\_dp:f:1.0326640429970195 | 154045         |


### All options
```
$ bash plasmidEC.sh -h
usage: bash plasmidEC.sh [-i INPUT] [-o OUTPUT] [options]

Mandatory arguments:
  -i INPUT              input .fasta or .gfa file
  -o OUTPUT             output directory

Optional arguments:
  -h			 Display this help message and exit.
  -n NAME		 Name for output files. (Default: Basename of input file).
  -l LENGTH		 Minimum length of contigs to be classified (Default: 1000)
  -g			 Write gplas2 formatted output.
  -m                     Use minority vote to classify contigs as plasmid-derived.
  -f			 Force overwriting of output directory.
  -v			 Display version and exit.

```

## Output Files

#### ensemble_output.csv
Main table containing the predictions made by each individual classifier, the total nr. of plasmid votes and the final classification for each contig.

```
head -n 5 E_coli_test/ensemble_output.csv
```

| Contig\_name                              | Genome\_id  | RFPlasmid  | Centrifuge | Platon     | Plasmid\_count | Combined\_prediction |
| ----------------------------------------- | ----------- | ---------- | ---------- | ---------- | -------------- | -------------------- |
| S1\_LN:i:346767\_dp:f:0.9966562474408179  | test\_ecoli | chromosome | chromosome | chromosome | 0              | chromosome           |
| S10\_LN:i:175297\_dp:f:0.9360667247742771 | test\_ecoli | chromosome | chromosome | chromosome | 0              | chromosome           |
| S100\_LN:i:1076\_dp:f:2.530236029051145   | test\_ecoli | chromosome | plasmid    | plasmid    | 2              | plasmid              |
| S101\_LN:i:1066\_dp:f:1.9988380278126159  | test\_ecoli | chromosome | chromosome | chromosome | 0              | chromosome           |

#### plasmid_contigs.fasta
Sequences of all contigs predicted to originate from plasmids in FASTA format.

```
grep '>' E_coli_test/plasmid_contigs.fasta
```
```
>S100_LN:i:1076_dp:f:2.530236029051145
>S37_LN:i:27545_dp:f:2.647645845987835
>S48_LN:i:11456_dp:f:1.4507819940318725
>S49_LN:i:10551_dp:f:1.3637191943487739
>S50_LN:i:9676_dp:f:1.5011735328773061
>S51_LN:i:9084_dp:f:2.6932550771872323
>S52_LN:i:9015_dp:f:2.60828989990341
>S54_LN:i:7604_dp:f:1.4886167382316031
>S57_LN:i:5200_dp:f:1.3890819886029557
>S66_LN:i:3439_dp:f:4.183650771596276
>S68_LN:i:3023_dp:f:2.589395047594386
>S71_LN:i:2870_dp:f:2.5799908536557314
>S72_LN:i:2817_dp:f:1.0382821435571183
>S74_LN:i:2600_dp:f:0.9782899464745628
>S76_LN:i:2354_dp:f:7.237618987589864
>S79_LN:i:2186_dp:f:1.0429091447800045
>S82_LN:i:1893_dp:f:1.3296811087334817
>S83_LN:i:1690_dp:f:3.7257036197991673
>S84_LN:i:1663_dp:f:1.3388510964667213
>S90_LN:i:1457_dp:f:2.515760144942872
>S94_LN:i:1316_dp:f:4.11423204293813
>S96_LN:i:1240_dp:f:1.415327303861984
>S98_LN:i:1164_dp:f:2.8587537599848356
>S99_LN:i:1076_dp:f:1.4379818048479307
```
### all_predictions.csv
Concatenated predictions of the individual classifiers (intermediate file).

```
head -n 5 E_coli_test/all_predictions.csv
```
|                                           |            |            |               |
| ----------------------------------------- | ---------- | ---------- | ------------- |
| S7\_LN:i:209197\_dp:f:1.060027589194678   | chromosome | centrifuge | E\_coli\_test |
| S1\_LN:i:346767\_dp:f:0.9966562474408179  | chromosome | centrifuge | E\_coli\_test |
| S10\_LN:i:175297\_dp:f:0.9360667247742771 | chromosome | centrifuge | E\_coli\_test |
| S9\_LN:i:197587\_dp:f:1.094028026559923   | chromosome | centrifuge | E\_coli\_test |
| S2\_LN:i:341820\_dp:f:1.0689129784313414  | chromosome | centrifuge | E\_coli\_test |

## Troubleshooting

Did you have problems running plasmidEC? Check below for some common sources of problems. 

You can also open an issue and we'll come back to you ASAP.
 
### Conda environments

PlasmidEC relies on a number of conda environments containing the individual tools and their dependencies (see [supported tools](#supported-tools)). 

These conda environments will be created upon first run. However, in some cases problems might arise during this step, alternatively, each environment can be installed separately with [mamba](https://github.com/mamba-org/mamba).

First, check if the environments have been properly installed by running:

```
conda env list
```
You should see the following conda environments:
```
plasmidEC_R
plasmidEC_centrifuge
plasmidEC_rfplasmid
plasmidEC_platon
```
If any of these environments are missing, use the following commands to install them with mamba:

```
mamba create --name plasmidEC_R -c bioconda -c conda-forge bioconductor-biostrings=2.60.0 r=4.1 r-tidyr=1.2.0 r-plyr=1.8.6 r-dplyr=1.0.7

mamba create --name plasmidEC_centrifuge -c bioconda centrifuge=1.0.4_beta

mamba create --name plasmidEC_platon -c bioconda -c conda-forge  platon=1.6

mamba create --name plasmidEC_rfplasmid -c bioconda -c conda-forge python=3.7 rfplasmid=0.0.18
conda activate plasmidEC_rfplasmid
rfplasmid --initialize
conda deactivate
```

### Database downloads

The databases that are necessary to run the prediction tools will be automatically downloaded during runtime. If preferred, they can also be downloaded before running plasmidEC and specified with the -p flag or saved to `databases/centrifuge` within the plasmidEC directory. The Centrifuge species-specific databases can be downloaded with the following commands:

The _E. coli_ database can be downloaded to `/databases/centrifuge` and should be saved as

```
wget -O E_coli_plasmid_db.tar.gz -P databases/centrifuge https://zenodo.org/record/1311641/files/chromosome_plasmid_db.tar.gz
```

The Platon database can be pre-downloaded as well (or will be downloaded during runtime) and should be placed into the `/databases/platon` subdirectory

```
wget -P databases/platon https://zenodo.org/record/4066768/files/db.tar.gz

```
## Reference

If you're using PlasmidEC, please cite: 

PlasmidEC and gplas2: An optimised short-read approach to predict and reconstruct antibiotic resistance plasmids in Escherichia coli.
Julian A Paganini, Jesse J Kerkvliet, Lisa Vader, Nienke L Plantinga, Rodrigo Meneses, Jukka Corander, Rob J.L. Willems, Sergio Arredondo-Alonso, Anita C Schurch.

bioRxiv 2023.08.31.555679; doi: https://doi.org/10.1101/2023.08.31.555679

## Acknowledgements

Lisa Vader: Original design, implementation and testing for _Escherichia coli_.

Julian Paganini: Gplas compatibility, implementation and testing.

Jesse Kerkvliet: Construction of Centrifuge databases, testing.

Anita Schürch: Design, testing and project supervision.
