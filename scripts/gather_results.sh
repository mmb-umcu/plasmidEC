#!/bin/bash

#set -x
set -e

while getopts :i:c:o: flag; do
        case $flag in
		i) input=$OPTARG;;
                c) classifiers=$OPTARG;;
		o) out_dir=$OPTARG;;
        esac
done

##MLPLASMIDS
gather_mlplasmids(){
file_list=$(ls $out_dir/mlplasmids_output/ | sed 's/.tsv//g')
for file in $file_list
do
if [ -f $out_dir/mlplasmids_output/$file.tsv ]; then
tail -n +2 $out_dir/mlplasmids_output/$file.tsv | while read line
do
prediction=$(echo "$line" | cut -f3 | sed 's/"//g')
contig=$(echo "$line" | cut -f4 | cut -f1 -d, | sed 's/"//g')
echo $contig,${prediction,,},mlplasmids,$file >> $out_dir/all_predictions.csv
done
else
echo "mlplasmids output couldn't be found" && exit 1
fi
done
}

##CENTRIFUGE
gather_centrifuge(){
results_dir=$out_dir/centrifuge_output
#go to extended result file to find contig ID and class nr
if [ -f $results_dir/*modified_centrifuge ]; then
cat $results_dir/*modified_centrifuge | sed '1d' | while read line
do
contig=$(echo "$line" | cut -f1)
class=$(echo "$line" | cut -f8)
#write to file
echo $contig,$class,"centrifuge",$file >> $out_dir/all_predictions.csv
done
else
echo "Centrifuge output couldn't be found. Check ${results_dir}/logs/centrifuge.log for issues" && exit 1
fi
}

##PLATON
gather_platon(){
results_dir=$out_dir/platon_output/*
#grab chromosomal contigs
if [ -f $results_dir/*chromosome.fasta ]; then
cat $results_dir/*chromosome.fasta | grep '>' | while read line
do
contig=$(echo "$line" | cut -c 2-)
echo $contig,"chromosome","platon",$file >> $out_dir/all_predictions.csv
done
#grab plasmid contigs
cat $results_dir/*plasmid.fasta | grep '>' | while read line
do
contig=$(echo "$line" | cut -c 2-)
echo $contig,"plasmid","platon",$file >> $out_dir/all_predictions.csv
done
else
echo "Platon output couldn't be found" && exit 1
fi
}

#RFPLASMID
gather_rfplasmid(){
dir=$(ls -Art $out_dir/rfplasmid_output | tail -n 1)
if [ -f $out_dir/rfplasmid_output/$dir/prediction.csv ]; then 
tail -n +2 $out_dir/rfplasmid_output/$dir/prediction.csv | while read line
do
#file=$(echo "$line" | cut -f 1 -d ',' | awk -F '_' 'BEGIN { OFS = FS }; NF { NF -= 1 }; 1' | sed 's/"//g')
contig=$(echo "$line" | cut -d, -f5 | sed 's/"//g')
if [[ $line = *'"p"'* ]]; then
echo $contig,"plasmid","rfplasmid",$file >> $out_dir/all_predictions.csv
else
echo $contig,"chromosome","rfplasmid",$file >> $out_dir/all_predictions.csv
fi
done
else
echo "RFPlasmids output was not found. Did you select a valid species model?" && exit 1
fi
}

file=$(basename $input .fasta)

if [[ $classifiers = *"mlplasmids"* ]]; then
	gather_mlplasmids
fi

if [[ $classifiers = *"centrifuge"* ]]; then
	gather_centrifuge
fi
if [[ $classifiers = *"platon"* ]]; then
	gather_platon
fi
if [[ $classifiers = *"rfplasmid"* ]]; then
	gather_rfplasmid
fi

