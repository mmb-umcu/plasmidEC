#!/bin/bash

#set -x
set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "../${BASH_SOURCE[1]}" )" &> /dev/null && pwd )

while getopts :i:o:t:d:n:s: flag; do
        case $flag in
                i) input=$OPTARG;;
                o) out_dir=$OPTARG;;
		t) threads=$OPTARG;;
		d) database_dir=$OPTARG;;
		n) centrifuge_database_name=$OPTARG;;
		s) species=$OPTARG;;
        esac
done

#make results directory
mkdir -p $out_dir/centrifuge_output

#make directory for saving pre-loaded databases
mkdir -p ${SCRIPT_DIR}/databases/centrifuge

#if species is E. coli
if [[ $species == 'Escherichia coli' ]]; then
if [[ ! -f ${database_dir}/${centrifuge_database_name}.3.cf ]]; then
	echo "Downloading PlaScope E. coli database..."
	wget -P ${database_dir} https://zenodo.org/record/1311641/files/${centrifuge_database_name}.tar.gz
	tar -xzf ${database_dir}/${centrifuge_database_name}.tar.gz -C ${database_dir}
	rm ${database_dir}/${centrifuge_database_name}.tar.gz
fi

#if species is K. pneumoniae
elif [[ $species == 'Klebsiella pneumoniae' ]]; then
if [[ ! -f ${database_dir}/${centrifuge_database_name}.3.cf ]]; then
        echo "Downloading Centrifuge K. pneumoniae database..."
        wget -P ${database_dir} https://zenodo.org/record/7194565/files/K_pneumoniae_plasmid_db.tar.gz
        tar -xzf ${database_dir}/K_pneumoniae_plasmid_db.tar.gz -C ${database_dir}
        rm ${database_dir}/K_pneumoniae_plasmid_db.tar.gz
fi

#if species is P. aeruginosa
elif [[ $species == 'Pseudomonas aeruginosa' ]]; then
if [[ ! -f ${database_dir}/${centrifuge_database_name}.3.cf ]]; then
        echo "Downloading Centrifuge P. aeruginosa database..."
        wget -P ${database_dir} https://zenodo.org/record/7133246/files/P_aeruginosa_plasmid_db.tar.gz
        tar -xzf ${database_dir}/P_aeruginosa_plasmid_db.tar.gz -C ${database_dir}
        rm ${database_dir}/P_aeruginosa_plasmid_db.tar.gz
fi

#if species is S. enterica
elif [[ $species == 'Salmonella enterica' ]]; then
echo "got the species"
if [[ ! -f ${database_dir}/${centrifuge_database_name}.3.cf ]]; then
        echo "Downloading Centrifuge S. enterica database..."
        wget -P ${database_dir} https://zenodo.org/record/7133407/files/S_enterica_plasmid_db.tar.gz
        tar -xzf ${database_dir}/S_enterica_plasmid_db.tar.gz -C ${database_dir}
        rm ${database_dir}/S_enterica_plasmid_db.tar.gz
fi

#if species is S. aureus
elif [[ $species == 'Staphylococcus aureus' ]]; then
if [[ ! -f ${database_dir}/${centrifuge_database_name}.3.cf ]]; then
        echo "Downloading Centrifuge S. enterica database..."
        wget -P ${database_dir} https://zenodo.org/record/7133406/files/S_aureus_plasmid_db.tar.gz
        tar -xzf ${database_dir}/S_aureus_plasmid_db.tar.gz -C ${database_dir}
        rm ${database_dir}/S_aureus_plasmid_db.tar.gz
fi

#if species is A. baumannii
elif [[ $species == 'Acinetobacter baumannii' ]]; then
if [[ ! -f ${database_dir}/${centrifuge_database_name}.3.cf ]]; then
        echo "Downloading Centrifuge A. baumannii database..."
        wget -P ${database_dir} https://zenodo.org/record/7326823/files/A_baumannii_plasmid_db.tar.gz
        tar -xzf ${database_dir}/A_baumannii_plasmid_db.tar.gz -C ${database_dir}
        rm ${database_dir}/A_baumannii_plasmid_db.tar.gz
fi



#if species in General
elif [[ $species == 'General' ]]; then
if [[ ! -f ${database_dir}/${centrifuge_database_name}.3.cf ]]; then
        echo "Downloading Centrifuge General database. This DB is 30GB, so it will take a while..."
        wget -P ${database_dir} https://zenodo.org/record/7431957/files/general_plasmid_db.tar.gz
        tar -xzf ${database_dir}/general_plasmid_db.tar.gz -C ${database_dir}
        rm ${database_dir}/general_plasmid_db.tar.gz
fi


fi

run_centrifuge(){
input=$1
out_dir=$2
threads=$3
centrifuge_database=$4

#run centrifuge on all strains in input directory
echo "Running Centrifuge..."
name=$(basename $input .fasta)
centrifuge -f --threads ${threads} -x ${centrifuge_database} -U ${input} -k 1000 --report-file ${out_dir}/centrifuge_output/${name}_summary -S ${out_dir}/centrifuge_output/${name}_extendedresults
}

echo -e "\nRunning Centrifuge..."
echo "Find logs and errors at ${out_dir}/logs/centrifuge*"

run_centrifuge $input $out_dir $threads ${database_dir}/${centrifuge_database_name} 1> ${out_dir}/logs/centrifuge_log.txt 2> ${out_dir}/logs/centrifuge_err.txt
